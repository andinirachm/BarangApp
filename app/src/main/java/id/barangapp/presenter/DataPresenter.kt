package id.barangapp.presenter

import id.barangapp.model.ItemDataPresenter
import id.barangapp.response.ItemItem
import id.barangapp.response.ItemModel
import id.barangapp.service.InitRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class DataPresenter {
    var dataPresenter: ItemDataPresenter? = null

    constructor(dataPresenter: ItemDataPresenter?) {
        this.dataPresenter = dataPresenter
    }

    fun getItem() {
        var api = InitRetrofit().instaceRetrofit()
        var call = api.getItem()
        call.enqueue(object : Callback<ItemModel> {
            override fun onFailure(call: Call<ItemModel>?, t: Throwable?) {
                dataPresenter?.onFailed(t?.message!!)
            }

            override fun onResponse(call: Call<ItemModel>?, response: Response<ItemModel>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        // get response
                        var result = response.body()?.result
                        var message = response.body()?.message
                        var item = response.body()?.item
                        dataPresenter?.onSuccess(result!!, message!!, (item as List<ItemItem>?)!!)
                    } else {
                        dataPresenter?.onFailed(response.errorBody().toString())
                    }
                }
            }

        });
    }


}