package id.barangapp.presenter

import android.util.Log
import id.barangapp.model.ItemPresenter
import id.barangapp.response.GlobalModel
import id.barangapp.service.InitRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class InsertPresenter {
    var modelPresenter: ItemPresenter? = null

    constructor(modelPresenter: ItemPresenter?) {
        this.modelPresenter = modelPresenter
    }

    fun insertItem(name: String, harga: String) {
        var api = InitRetrofit().instaceRetrofit()
        var call = api.addItem(name, harga)
        call.enqueue(object : Callback<GlobalModel> {
            override fun onFailure(call: Call<GlobalModel>?, t: Throwable?) {
                Log.d("KOTLIN", t?.message)
                modelPresenter?.onFailed(t?.message!!)
            }

            override fun onResponse(call: Call<GlobalModel>?, response: Response<GlobalModel>?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                Log.d("KOTLIN", response?.message())
                if (response != null) {
                    if (response.isSuccessful) {
                        // get response
                        var result = response.body()?.result
                        var message = response.body()?.message
                        modelPresenter?.onSuccess(result!!, message!!)
                    } else {
                        modelPresenter?.onFailed(response.errorBody().toString())
                    }
                }
            }

        })
    }




}