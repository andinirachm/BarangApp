package id.barangapp

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.barangapp.view.Fragment1
import id.barangapp.view.Fragment2

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class TabPagerAdapter : FragmentPagerAdapter {

    constructor(fm: FragmentManager?) : super(fm)


    override fun getItem(position: Int): Fragment {
        if (position == 0)
            return Fragment1()
        else
            return Fragment2()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        if (position == 0)
            return "Left"
        else
            return "Right"
    }

    fun getView(position: Int) {

    }
}