package id.barangapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tab_layout.addTab(tab_layout.newTab().setText("Left"))
        tab_layout.addTab(tab_layout.newTab().setText("Right"))

        var adapter = TabPagerAdapter(supportFragmentManager)
        view_pager.adapter = adapter
        tab_layout.setupWithViewPager(view_pager)

       /* val tv = (tab_layout.getChildAt(0) as LinearLayout) as TextView
        val face = Typeface.createFromAsset(getAssets(), "CircularStd-Book.otf");
        tv.setTypeface(face)*/
    }
}
