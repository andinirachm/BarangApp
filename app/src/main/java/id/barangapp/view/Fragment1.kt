package id.barangapp.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.barangapp.model.ItemPresenter
import id.barangapp.presenter.InsertPresenter
import kotlinx.android.synthetic.main.fragment_1.*
import id.barangapp.R

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class Fragment1 : Fragment(), ItemPresenter {
    var presenter: InsertPresenter? = null
    override fun onSuccess(status: Boolean, message: String) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

        if (status) {
           /* var intent = Intent(activity, MainActivity::class.java)//MainActivity::class.java
            startActivity(intent)*/
        }
        else{

        }
    }

    override fun onFailed(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_1, container, false)
        presenter = InsertPresenter(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        btn_add.setOnClickListener {
            if (edit_text_nama.text.isNotEmpty() && edit_text_harga.text.isNotEmpty()) {
                presenter!!.insertItem(edit_text_nama.text.toString(), edit_text_harga.text.toString())
            }
        }
    }
}