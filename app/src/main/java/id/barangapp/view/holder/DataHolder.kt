package id.barangapp.view.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import id.barangapp.response.ItemItem
import kotlinx.android.synthetic.main.item_data.view.*

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class DataHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    fun bindData(item: ItemItem) {
        with(item)
        {
            itemView.text_view_nama.setText(item.namaBarang.toString())
            itemView.text_view_harga.setText(item.hargaBarang.toString())
        }

    }
}