package id.barangapp.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import id.barangapp.MainActivity
import id.barangapp.R

class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        var handler = Handler()
        handler.postDelayed(Runnable {
            finish()
            if (session?.getPreferences("email")?.length == 0 || session?.getPreferences("email") == null) {
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }, 3000)
    }
}
