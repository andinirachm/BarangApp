package id.barangapp.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.barangapp.helper.SessionManager


/**
 * Created by Andini Rachmah on 9/20/2017.
 */
open class BaseActivity : AppCompatActivity() {
    var session: SessionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = SessionManager(this)
    }

    fun intent(classs: Class<BaseActivity>) {
        var intent = Intent(this, classs)
        startActivity(intent)
    }
}