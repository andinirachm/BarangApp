package id.barangapp.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import id.barangapp.MainActivity
import id.barangapp.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_sign_in.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var email = edit_text_email.text.toString()
        var password = edit_text_password.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            session?.setPreferences("email", email)
            session?.setPreferences("password", password)

            Toast.makeText(this, "Success login", Toast.LENGTH_SHORT).show()

            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }

    }
}


