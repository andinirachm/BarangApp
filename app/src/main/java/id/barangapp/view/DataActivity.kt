package id.barangapp.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.barangapp.R
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        edit_text_nama.setText(intent.getStringExtra("nama").toString())
        edit_text_harga.setText(intent.getStringExtra("harga").toString())

        btn_update.setOnClickListener {

        }
    }
}
