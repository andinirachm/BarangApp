package id.barangapp.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.barangapp.R
import id.barangapp.model.ItemDataPresenter
import id.barangapp.presenter.DataPresenter
import id.barangapp.response.ItemItem
import id.barangapp.view.adapter.DataAdapter
import kotlinx.android.synthetic.main.fragment_2.*
/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class Fragment2 : Fragment(), ItemDataPresenter, SwipeRefreshLayout.OnRefreshListener {
    var presenter: DataPresenter? = null
    var dataAdapter: DataAdapter? = null

    override fun onRefresh() {
        presenter?.getItem()
        swipe_refresh.isRefreshing = false
    }

    override fun onSuccess(status: Boolean, message: String, data: List<ItemItem>) {
        dataAdapter?.listData?.clear()
        dataAdapter?.listData?.addAll(data)
        dataAdapter?.notifyDataSetChanged()
    }

    override fun onFailed(message: String) {

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.fragment_2, container, false);
        presenter = DataPresenter(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        swipe_refresh.setOnRefreshListener(this)
        recycler_view.layoutManager = GridLayoutManager(activity, 1)
        dataAdapter = DataAdapter(activity)
        recycler_view.adapter = dataAdapter
    }

    override fun onResume() {
        super.onResume()
        presenter?.getItem()

    }

    companion object {
    }

    fun getData() {
        presenter?.getItem()
    }

}