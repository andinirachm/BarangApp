package id.barangapp.view.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.barangapp.R
import id.barangapp.response.ItemItem
import id.barangapp.view.DataActivity
import id.barangapp.view.holder.DataHolder

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class DataAdapter : RecyclerView.Adapter<DataHolder> {
    var context: Context? = null
    var listData: ArrayList<ItemItem>? = ArrayList()

    constructor(context: Context?) {
        this.context = context
    }

    override fun getItemCount(): Int {
        return listData?.size!!
    }

    override fun onBindViewHolder(holder: DataHolder?, position: Int) {
        var item = listData?.get(position)
        holder?.bindData(item as ItemItem)
        holder?.itemView?.setOnClickListener {
            var intent = Intent(context, DataActivity::class.java)
            intent.putExtra("nama", item?.namaBarang)
            intent.putExtra("harga", item?.hargaBarang)
            context?.startActivity(intent)
        }
        holder?.itemView?.setOnLongClickListener {


            true
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DataHolder {
        var itemView = LayoutInflater.from(context).inflate(R.layout.item_data, parent, false)
        var dataHolder = DataHolder(itemView)
        return dataHolder
    }
}