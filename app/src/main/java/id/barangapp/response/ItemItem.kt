package id.barangapp.response

import com.google.gson.annotations.SerializedName

data class ItemItem(

	@field:SerializedName("id_barang")
	val idBarang: String? = null,

	@field:SerializedName("nama_barang")
	val namaBarang: String? = null,

	@field:SerializedName("harga_barang")
	val hargaBarang: String? = null
)