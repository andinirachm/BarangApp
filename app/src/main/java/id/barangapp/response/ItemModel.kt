package id.barangapp.response

import com.google.gson.annotations.SerializedName

data class ItemModel(

	@field:SerializedName("result")
	val result: Boolean? = null,

	@field:SerializedName("item")
	val item: List<ItemItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)