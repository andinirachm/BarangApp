package id.barangapp.helper

import android.content.Context

class SessionManager {
    var context: Context? = null

    constructor(context: Context?) {
        this.context = context
    }

    fun setPreferences(key: String, value: String) {
        val editor = context?.getSharedPreferences("Netz", Context.MODE_PRIVATE)?.edit()
        editor?.putString(key, value)
        editor?.commit()
    }

    fun getPreferences(key: String): String {
        val prefs = context?.getSharedPreferences("Netz", Context.MODE_PRIVATE)
        val position = prefs?.getString(key, "")
        return position!!
    }
}