package id.barangapp.model

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
interface ItemPresenter {
    fun onSuccess(status: Boolean, message: String)
    fun onFailed(message: String)
}