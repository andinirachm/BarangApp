package id.barangapp.model

import id.barangapp.response.ItemItem

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
interface ItemDataPresenter {
    fun onSuccess(status: Boolean, message: String, data: List<ItemItem>)
    fun onFailed(message: String)
}