package id.barangapp.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
class InitRetrofit {
    fun initialize(): Retrofit {
        return Retrofit.Builder().baseUrl("http://192.168.20.220/server_barang/index.php/Api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    fun instaceRetrofit(): APIService {
        return initialize().create(APIService::class.java)
    }
}