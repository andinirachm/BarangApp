package id.barangapp.service

import id.barangapp.response.GlobalModel
import id.barangapp.response.ItemModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by Andini Rachmah on 9/19/2017.
 */
interface APIService {

    @FormUrlEncoded
    @POST("AddItem") //function di API
    fun addItem(@Field("nama") nama: String, @Field("harga") harga: String): Call<GlobalModel>

    @GET("GetItem")
    fun getItem(): Call<ItemModel>

    @FormUrlEncoded
    @POST("Delete")
    fun deleteItem(@Field("id") id: String): Call<ItemModel>

    @FormUrlEncoded
    @POST("UpdateItem")
    fun updateItem(@Field("nama") nama: String, @Field("harga") harga: String): Call<GlobalModel>
}